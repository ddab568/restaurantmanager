package com.restaurantManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * @author Dana Dabbagh
 */
public class RestaurantManagerTest
{

	/**
	 * Restaurant Manager
	 **/
	private final RestaurantManager m_restaurantManager = new RestaurantManager();

	/**
	 * To capture System.out and System.err for testing
	 **/
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

	private int NUM_TABLES = Restaurant.getNumTables();


	@Before
	public void setUp()
	{
		System.setOut(new PrintStream(outContent));
		System.setErr(new PrintStream(errContent));
	}

	/**
	 * Test that initial manager view shows unassigned tables for the restaurants
	 */
	@Test
	public void testBasicManagerView()
	{
		int restaurantOne = 1;
		int restaurantTwo = 2;

		m_restaurantManager.getManagerView(restaurantOne);

		StringBuilder managerViewOutput = new StringBuilder();

		for (int i = 1; i <= NUM_TABLES; i++)
		{
			managerViewOutput.append("Table ").append(i).append(": Unassigned").append(System.lineSeparator());
		}
		assertEquals(managerViewOutput.toString(), outContent.toString());

		outContent.reset();

		m_restaurantManager.getManagerView(restaurantTwo);

		managerViewOutput = new StringBuilder();

		for (int i = 1; i <= NUM_TABLES; i++)
		{
			managerViewOutput.append("Table ").append(i).append(": Unassigned").append(System.lineSeparator());
		}
		assertEquals(managerViewOutput.toString(), outContent.toString());
	}

	/**
	 * Show that assigning a waiter to a restaurant
	 * will show that the table is assigned to that waiter
	 * when calling manager view
	 */
	@Test
	public void testBasicAssign()
	{
		int restaurant = 1;
		int waiter = 1;
		int tableNumber = 1;

		m_restaurantManager.assignWaiter(restaurant, waiter, tableNumber);
		m_restaurantManager.getManagerView(restaurant);

		String waiterName = m_restaurantManager.getWaiterName(waiter);
		assert (outContent.toString().contains("Table 1: " + waiterName));
	}

	/**
	 * Show that assigning and then unassigning a waiter will
	 * reset manager view to all tables unassigned
	 */
	@Test
	public void testBasicUnassign()
	{
		int restaurant = 1;
		int waiter = 1;
		int tableNumber = 1;

		m_restaurantManager.assignWaiter(restaurant, waiter, tableNumber);
		m_restaurantManager.unassignWaiter(restaurant, waiter, tableNumber);

		outContent.reset();
		m_restaurantManager.getManagerView(restaurant);

		StringBuilder managerViewOutput = new StringBuilder();

		for (int i = 1; i <= NUM_TABLES; i++)
		{
			managerViewOutput.append("Table ").append(i).append(": Unassigned").append(System.lineSeparator());
		}
		assertEquals(managerViewOutput.toString(), outContent.toString());
	}

	/**
	 * Show that assigning a table to a waiter
	 * will display the table when calling WaiterView
	 * on that waiter
	 */
	@Test
	public void testBasicWaiterView()
	{
		int restaurant = 1;
		int waiter = 1;
		int tableNumber = 1;

		m_restaurantManager.assignWaiter(restaurant, waiter, tableNumber);

		outContent.reset();
		m_restaurantManager.getWaiterView(restaurant, waiter);

		String waiterViewOutput = m_restaurantManager.getWaiterName(waiter) + ":" +
				System.lineSeparator() + "1. Table 1" + System.lineSeparator();
		assertEquals(waiterViewOutput, outContent.toString());
	}

	/**
	 * Show that assigning more than four tables to a waiter
	 * will produce an error
	 */
	@Test
	public void testMaxAssign() throws IOException
	{
		int restaurant = 1;
		int waiter = 2;

		for (int tableNumber = 1; tableNumber <= 5; tableNumber++)
		{
			m_restaurantManager.assignWaiter(restaurant, waiter, tableNumber);
		}

		String expectedError = "Can't assign more than 4 tables to a waiter" + System.lineSeparator();
		assertEquals(expectedError, errContent.toString());
	}

	/**
	 * Delete restaurant files
	 */
	private void deleteRestaurantFiles() throws IOException
	{
		Files.delete(Paths.get("RestaurantOne.ser"));
		Files.delete(Paths.get("RestaurantTwo.ser"));
	}

	/**
	 * Clean up
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@After
	public void tearDownClass() throws IOException, InterruptedException
	{
		deleteRestaurantFiles();
		System.setOut(null);
		System.setErr(null);
	}
}
