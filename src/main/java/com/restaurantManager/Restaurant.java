package com.restaurantManager;

import java.io.*;

/**
 * The handler for the individual restaurant.
 * Will ensure all action is persisted on file.
 *
 * @author Dana Dabbagh
 */
class Restaurant implements Serializable
{
	/** The serialized file **/
	private File m_restaurantFile;

	/** The number of tables **/
	private static int NUM_TABLES = 20;

	/**
	 * Creates a new serialized file if one doesn't
	 * already exist, otherwise will load existing
	 * @param name restaurant name
	 */
	public Restaurant(String name)
	{
		m_restaurantFile = new File( name + ".ser");

		if (m_restaurantFile.exists())
		{
			loadFromExistingFile();
		} else
		{
			createNewFile();
		}
	}

	/**
	 * Loads the existing restaurant file
	 * and updates the waiter assignments
	 */
	private void loadFromExistingFile()
	{
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(m_restaurantFile)))
		{
			Table table = (Table) ois.readObject();
			while (table != null)
			{
				Waiter currentWaiter = table.getAssignedWaiter();

				if (currentWaiter != null)
				{
					Waiter waiter = RestaurantManager.s_waiters[currentWaiter.getWaiterNumber()];
					waiter.incrementTablesWaiting();
				}

				table = (Table) ois.readObject();
			}
		} catch (EOFException eof)
		{
			//done
		} catch (IOException | ClassNotFoundException e)
		{
			e.printStackTrace();

		}
	}

	/**
	 * Creates a new restaurant file
	 */
	private void createNewFile()
	{
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(m_restaurantFile)))
		{
			for (int i = 1; i <= NUM_TABLES; i++)
			{
				oos.writeObject(new Table(i));
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Re-writes the entire restaurant file to a temp file, updating
	 * the table which is chosen to be assigned. Original file
	 * will then be replaced by the temp file.
	 * @param waiter the waiter to assign
	 * @param tableNumber the table number to assign to
	 */
	void assignWaiter(Waiter waiter, int tableNumber)
	{

		File tempFile = new File(m_restaurantFile.getName() + "tmp");
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(m_restaurantFile)))
		{
			try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(tempFile)))
			{
				int currentTableNumber = 1;
				Table table = (Table) ois.readObject();
				while (table != null)
				{
					if (currentTableNumber == tableNumber)
					{
						if (table.getAssignedWaiter() == null)
						{
							table.assignWaiter(waiter);
							waiter.incrementTablesWaiting();
						} else
						{
							System.err.println("This table is already being waited by " + table.getAssignedWaiter());
						}
					}
					oos.writeObject(table);
					currentTableNumber++;
					table = (Table) ois.readObject();
				}
			}
		} catch (EOFException e)
		{
			//done

		} catch (IOException | ClassNotFoundException e)
		{
			e.printStackTrace();
		} finally
		{
			if (!m_restaurantFile.delete() || !tempFile.renameTo(m_restaurantFile))
			{
				System.out.println("Could not replace " + m_restaurantFile + " with " + tempFile);
			}

		}
	}

	/**
	 * Re-writes the entire restaurant file to a temp file, updating
	 * the table which is chosen to be unassigned. Original file
	 * will then be replaced by the temp file.
	 * @param waiter the waiter to unassign
	 * @param tableNumber the table number to unassign
	 */
	void unassignWaiter(int tableNumber, Waiter waiter)
	{
		File tempFile = new File(m_restaurantFile.getName() + "tmp");
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(m_restaurantFile)))
		{
			try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(tempFile)))
			{
				for (int i = 1; i <= NUM_TABLES; i++)
				{
					Table table = (Table) ois.readObject();
					if (i == tableNumber)
					{
						if (table.getAssignedWaiter().equals(waiter))
						{
							table.assignWaiter(null);
							waiter.decrementTablesWaiting();
						} else
						{
							System.err.println(table + " is assigned to " + waiter);
						}
					}

					oos.writeObject(table);
				}
			}
		} catch (IOException | ClassNotFoundException e)
		{
			e.printStackTrace();
		} finally
		{
			if( !m_restaurantFile.delete() || ! tempFile.renameTo(m_restaurantFile))
			{
				System.err.println("Could not replace " + m_restaurantFile + " with " + tempFile );
			}

		}
	}

	static int getNumTables()
	{
		return NUM_TABLES;
	}

	/**
	 * Will display the tables assigned to the waiter
	 * @param waiter the specified waiter
	 */
	void printAssignedTables(Waiter waiter)
	{
		System.out.println(waiter + ":");
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(m_restaurantFile)))
		{
			Table table = (Table) ois.readObject();
			while (table != null)
			{
				if (table.getAssignedWaiter() != null
						&& table.getAssignedWaiter().equals(waiter))
				{
					System.out.println(table.getTableNumber() + ". " + table);
				}
				table = (Table) ois.readObject();
			}
		} catch (EOFException eof)
		{
			//done
		} catch (IOException | ClassNotFoundException e)
		{
			e.printStackTrace();

		}
	}

	/**
	 * Prints only the tables which aren't yet assigned
	 */
	void printUnassignedTables()
	{
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(m_restaurantFile)))
		{
			Table table = (Table) ois.readObject();
			while (table != null)
			{

				if (table.getAssignedWaiter() == null)
				{
					System.out.println(table.getTableNumber() + ". " + table);
				}
				table = (Table) ois.readObject();
			}
		} catch (EOFException eof)
		{
			//done
		} catch (IOException | ClassNotFoundException e)
		{
			e.printStackTrace();

		}
	}

	/**
	 * Displays the 'Manager view'
	 * of the system
	 */
	void getManagerView()
	{
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(m_restaurantFile)))
		{
			Table table = (Table) ois.readObject();
			while (table != null)
			{
				System.out.println(table + ": " + (table.getAssignedWaiter() == null ? "Unassigned" : table
						.getAssignedWaiter()));
				table = (Table) ois.readObject();
			}
		} catch (EOFException eof)
		{
			//done
		} catch (IOException | ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * @author Dana Dabbagh
	 *
	 * The table class maintains information
	 * about each table in the restaurant
	 * and who is currently assigned to it
	 */
	private class Table implements Serializable
	{
		private int m_tableNumber;
		private Waiter m_assignedWaiter;

		Table(int number)
		{
			m_tableNumber = number;
			m_assignedWaiter = null;
		}

		int getTableNumber()
		{
			return m_tableNumber;
		}

		Waiter getAssignedWaiter()
		{
			return m_assignedWaiter;
		}

		void assignWaiter(Waiter waiter)
		{
			m_assignedWaiter = waiter;
		}

		@Override
		public String toString()
		{
			return "Table " + m_tableNumber;
		}

	}

}
