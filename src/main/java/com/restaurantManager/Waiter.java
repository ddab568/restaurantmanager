package com.restaurantManager;

import java.io.Serializable;

/**
 * The Waiter object
 * @author Dana Dabbagh
 *
 */
class Waiter implements Serializable
{
	private String m_name;
	private final int m_waiterNumber;
	private int m_tablesWaiting;

	public Waiter(String name, int number)
	{
		m_name = name;
		m_waiterNumber = number;
	}

	int getWaiterNumber()
	{
		return m_waiterNumber;
	}

	int getTablesWaiting()
	{
		return m_tablesWaiting;
	}

	void incrementTablesWaiting()
	{
		m_tablesWaiting++;
	}

	void decrementTablesWaiting()
	{
		m_tablesWaiting--;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Waiter waiter = (Waiter) o;

		return m_name != null ? m_name.equals(waiter.m_name) : waiter.m_name == null;

	}

	@Override
	public int hashCode()
	{
		return m_name != null ? m_name.hashCode() : 0;
	}

	@Override
	public String toString()
	{
		return m_name;
	}

}