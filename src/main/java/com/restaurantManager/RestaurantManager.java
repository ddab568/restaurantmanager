package com.restaurantManager;

/**
 * The interface between user input and the two restaurants.
 * @author Dana Dabbagh
 */
public class RestaurantManager
{
	/** Restaurants**/
	private Restaurant[] m_restaurants;

	/** Waiters **/
	static Waiter[] s_waiters;

	/** Constants as per specification **/
	private final static int NUM_RESTAURANTS = 2;
	private final static int NUM_WAITERS = 8;
	private final static int MAX_TABLES = 4;

	static
	{
		//Starting index will be 1
		s_waiters = new Waiter[NUM_WAITERS + 1];

		s_waiters[1] = new Waiter("Frank Lewis", 1);
		s_waiters[2] = new Waiter("Larry Hill", 2);
		s_waiters[3] = new Waiter("Jack Peterson", 3);
		s_waiters[4] = new Waiter("Julia Phillips", 4);
		s_waiters[5] = new Waiter("Sarah Morgan", 5);
		s_waiters[6] = new Waiter("Jacqueline Taylor", 6);
		s_waiters[7] = new Waiter("Will Ted", 7);
		s_waiters[8] = new Waiter("Jeremy Rogers", 8);
	}

	public RestaurantManager()
	{
		//Starting index will be 1
		m_restaurants = new Restaurant[NUM_RESTAURANTS + 1];
		m_restaurants[1] = new Restaurant("RestaurantOne");
		m_restaurants[2] = new Restaurant("RestaurantTwo");
	}

	/** Validate user input according to bounds **/
	public boolean validInput(int restaurant, int waiter, int tableNumber)
	{
		if (restaurant < 1 || restaurant > NUM_RESTAURANTS)
		{
			System.err.println("Invalid restaurant. Please choose either 1 or 2");
			return false;
		}

		if (waiter < 1 || waiter > NUM_WAITERS)
		{
			System.err.println("Invalid waiter. Please select a number between 1 and " + NUM_WAITERS);
			return false;
		}

		if (tableNumber < 1 || tableNumber > Restaurant.getNumTables())
		{
			System.err.println("Invalid table. Please select a number between 1 and " + Restaurant.getNumTables());
			return false;
		}

		if (s_waiters[waiter].getTablesWaiting() == MAX_TABLES)
		{
			System.err.println("Can't assign more than " + MAX_TABLES + " tables to a waiter");
			return false;
		}

		return true;
	}

	/**
	 * Assign a waiter to a specified table at a restaurant.
	 * @param restaurant the restaurant number corresponding to the menu options
	 * @param waiter the waiter number corresponding to the menu options
	 * @param tableNumber the table number corresponding to the menu options
	 */
	public void assignWaiter(int restaurant, int waiter, int tableNumber)
	{
		if(!validInput(restaurant,waiter,tableNumber)) { return;}

		System.out.println("Assigning " + s_waiters[waiter] + " to table " + tableNumber + " at restaurant " +
				restaurant);

		m_restaurants[restaurant].assignWaiter(s_waiters[waiter], tableNumber);

	}

	/**
	 * Unassign a waiter from a specified table at a restaurant.
	 * @param restaurant the restaurant number corresponding to the menu options
	 * @param waiter the waiter number corresponding to the menu options
	 * @param tableNumber the table number corresponding to the menu options
	 */
	public void unassignWaiter(int restaurant, int waiter, int tableNumber)
	{
		if(!validInput(restaurant,waiter,tableNumber)) { return;}
		System.out.println("Unassigning " + s_waiters[waiter] + " from table " + tableNumber + " at restaurant " +
				restaurant);
		m_restaurants[restaurant].unassignWaiter(tableNumber, s_waiters[waiter]);
	}

	/**
	 * Display the 'Manager view'
	 * @param restaurant The restaurant to display
	 */
	public void getManagerView(int restaurant)
	{
		m_restaurants[restaurant].getManagerView();
	}

	public void getWaiterView(int restaurantNumber, int waiterNumber)
	{
		displayAssignedTables(restaurantNumber, waiterNumber);
	}

	/**
	 * Display all the waiters
	 */
	public void displayWaiters()
	{
		for (int i = 1; i < s_waiters.length; i++)
		{
			System.out.println(i + "." + s_waiters[i]);
		}
	}

	/**
	 * Display all the tables assigned to the specified waiter at a restaurant
	 * @param restaurant The restaurant
	 * @param waiterNumber the waiter
	 */
	public void displayAssignedTables(int restaurant, int waiterNumber)
	{
		m_restaurants[restaurant].printAssignedTables(s_waiters[waiterNumber]);
	}

	/**
	 * Display all the unassigned tables at a restaurant
	 * @param restaurant The restaurant
	 */
	public void displayUnassignedTables(int restaurant)
	{
		if (restaurant < 1 || restaurant > NUM_RESTAURANTS )
		{
			System.err.println("Please select a restaurant between 1-2");
			return;
		}
		m_restaurants[restaurant].printUnassignedTables();
	}

	/**
	 * Returns whether the waiter is currently assigned to any tables
	 * @param waiter the waiter
	 * @return True if tables are assigned to the waiter, false otherwise
	 */
	public boolean isAssignedWaiter(int waiter)
	{
		return s_waiters[waiter].getTablesWaiting() > 0;
	}

	static int getNumRestaurants()
	{
		return NUM_RESTAURANTS;
	}

	static int getNumWaiters()
	{
		return NUM_WAITERS;
	}

	static int getMaxTables()
	{
		return MAX_TABLES;
	}

	String getWaiterName(int num)
	{
		return s_waiters[num].toString();
	}
}
