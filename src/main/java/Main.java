import com.restaurantManager.RestaurantManager;

import java.util.Scanner;

/**
 * Takes the input from the user through command
 * line and interfaces with RestaurantManager
 * to perform a range of actions
 *
 * @author Dana Dabbagh
 */
public class Main
{
	/** User input constants **/
	private final static int ASSIGN = 1;
	private final static int UNASSIGN = 2;
	private final static int MANAGER_VIEW = 3;
	private final static int WAITER_VIEW = 4;
	private final static int QUIT = 5;

	/** RestaurantManager **/
	private final static RestaurantManager s_restaurantManager = new RestaurantManager();

	static public void main(String[] args)
	{
		System.out.println("Welcome to Restaurant Manager");

		Scanner in = new Scanner(System.in);
		int input = 0;

		while (input != QUIT)
		{
			printOptions();

			try
			{
				input = in.nextInt();

				switch (input)
				{
					case ASSIGN:
						displayAssignPrompt(in);
						break;
					case UNASSIGN:
						displayUnassignPrompt(in);
						break;
					case MANAGER_VIEW:
						displayManagerViewPrompt(in);
						break;
					case WAITER_VIEW:
						displayWaiterViewPrompt(in);
						break;
					case QUIT:
						break;
					default:
						System.err.println("Please enter a number between 1 and 5 corresponding to an action");
						break;
				}

			} catch (Exception e)
			{
				//Catch any incorrect input or errors and continue
				System.err.println("Error: " + e.getMessage());
				in.next();
			}
		}
	}

	/**
	 * Print main options
	 */
	private static void printOptions()
	{
		String prompt = "Enter the number corresponding to an action: \n" +
				"1: Assign waiter\n" +
				"2: Unassign waiter\n" +
				"3: Manager View\n" +
				"4: Waiter View\n" +
				"5. Quit\n ";

		System.out.println(prompt);
	}

	/**
	 * Display the restaurant selection prompt
	 */
	private static void displayRestaurantPrompt()
	{
		System.out.println("Select a restaurant:\n" +
				"1. Restaurant One\n" +
				"2. Restaurant Two");
	}

	/**
	 * Assigns a selected waiter to a specified table and restaurant
	 * @param in Scanner to be used for more input
	 */
	private static void displayAssignPrompt(Scanner in)
	{
		displayRestaurantPrompt();
		int restaurant = in.nextInt();

		System.out.println("Select a waiter:");
		s_restaurantManager.displayWaiters();
		int waiter = in.nextInt();

		System.out.println("Select a table:");
		s_restaurantManager.displayUnassignedTables(restaurant);
		int table = in.nextInt();
		s_restaurantManager.assignWaiter(restaurant, waiter, table);
	}

	/**
	 * Unassigns a selected waiter to a previously assigned table at a restaurant
	 * @param in Scanner to be used for more input
	 */
	private static void displayUnassignPrompt(Scanner in)
	{
		displayRestaurantPrompt();
		int restaurant = in.nextInt();

		System.out.println("Select a waiter:");
		s_restaurantManager.displayWaiters();
		int waiter = in.nextInt();

		if (!s_restaurantManager.isAssignedWaiter(waiter))
		{
			System.err.println("Waiter is not assigned to any tables! ");
			return;
		}

		System.out.println("Select a table:");
		s_restaurantManager.displayAssignedTables(restaurant, waiter);
		int table = in.nextInt();

		s_restaurantManager.unassignWaiter(restaurant, waiter, table);
	}

	/**
	 * Displays the 'Waiter View' or tables assigned to a specified
	 * waiter at a specified restaurant
	 * @param in Scanner for continued input
	 */
	private static void displayWaiterViewPrompt(Scanner in)
	{
		displayRestaurantPrompt();
		int restaurant = in.nextInt();

		System.out.println("Select a waiter:");
		s_restaurantManager.displayWaiters();
		int waiter = in.nextInt();

		s_restaurantManager.getWaiterView(restaurant, waiter);
	}

	/**
	 * Displays the 'Manager view' which is the output of all
	 * the tables and their assigned waiter at a specified restaurant
	 * @param in Scanner for continued input
	 */
	private static void displayManagerViewPrompt(Scanner in)
	{
		displayRestaurantPrompt();
		int restaurant = in.nextInt();
		s_restaurantManager.getManagerView(restaurant);
	}

}
